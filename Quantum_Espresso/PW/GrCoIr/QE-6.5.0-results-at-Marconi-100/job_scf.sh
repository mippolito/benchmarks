#!/bin/bash -x
#SBATCH --job-name="QE-std"
#SBATCH --time=04:00:00
#SBATCH --nodes=36
#SBATCH --ntasks-per-node=4
#SBATCH --ntasks-per-socket=2
#SBATCH --cpus-per-task=32
#SBATCH --mem=230GB
#SBATCH --account=IscrC_ProBottY_0
#SBATCH --partition=m100_usr_prod
#SBATCH --qos=m100_qos_bprod
#SBATCH --gres=gpu:4

nodes=$SLURM_JOB_NUM_NODES
ntasks=$SLURM_NTASKS
nthreads=$SLURM_CPUS_PER_TASK
npe=$(( 32 / SLURM_NTASKS_PER_NODE ))
#
#npool=$ntasks
npool=4
#
myhome=/m100/home/userexternal/aferrett
source $myhome/mymodules_pgi_spec.sh
source $myhome/mylibs_pgi_spec.sh

para_prefix="mpirun -np $ntasks --map-by socket:PE=${npe} --rank-by core $myhome/gpu_bind.sh "
para_postfix="-npool $npool"
export OMP_NUM_THREADS=$nthreads

jobdir=$myhome/systems/GrCoIr
bindir=$myhome/codes/qe-6.5-gpu-spec/bin

#
# running
#
cd $jobdir

filein=scf_GrCoIr_ndim2.in
fileout=scf_GrCoIr_ndim2.out__MPI${ntasks}_OMP0_POOL${npool}
#
$para_prefix $bindir/pw.x $para_postfix -i $filein > $fileout

